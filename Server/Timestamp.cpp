#include "Timestamp.h"

CTimestampGuard::CTimestampGuard(const std::string & commandType)
	: m_nBytes(0)
{
	m_start = clock();
	std::cout << commandType << std::endl;
}

CTimestampGuard::~CTimestampGuard()
{
	clock_t timePassed = clock() - m_start;
	std::cout << "Request processed in " << timePassed << " ms." << std::endl;
	if (m_nBytes > 0)
	{
		uint64_t nBytesPerSecond = m_nBytes * 1000 / timePassed;
		uint64_t nKbPerSecond = (nBytesPerSecond >> 10);
		if (nKbPerSecond > 0)
			std::cout << nKbPerSecond << " Kb per second" << std::endl;
		else
			std::cout << nBytesPerSecond << " bytes per second" << std::endl;
	}
}

void CTimestampGuard::SetBytes(uint64_t nBytes)
{
	m_nBytes = nBytes;
}
