#pragma once
#include "Common.h"

class CSocket
{
public:
	bool Recv(void * pBuffer, size_t bufferSize);
	bool Recv(std::string & buffer, char terminator);
	virtual bool RecvFile(const std::string & filename, size_t & receivedPortions) = 0;

	bool Send(const void * buffer, size_t size);
	bool Send(const std::string & message);
	virtual bool SendFile(const std::string & filename, size_t & sentPortions) = 0;

	virtual ~CSocket() = default;

protected:
	int SetTimeouts(SOCKET sock, unsigned int seconds);

private:
	virtual int RecvImpl(char * buf, int len, int flags) const = 0;
	virtual int SendImpl(const char * buf, int len, int flags) const = 0;

};
