#include "Common.h"
#include "Socket.h"
#include "ClientProcessor.h"
#include "Client.h"
#include "UdpSocket.h"
#include "TcpSocket.h"

void UdpServerMain()
{
	std::string portClientStr = "5555";
	std::string portServerStr = "5556";

	/*
	std::cout << "Enter server port " << std::endl;
	std::cin << portServerStr;
	std::cout << "Enter client port " << std::endl;
	std::cin << portClientStr;
	*/
	auto portClient = unsigned short(std::stoul(portClientStr));
	auto portServer = unsigned short(std::stoul(portServerStr));
	std::unique_ptr<CSocket> sock(new CUdpSocket(portClient, portServer));
	CClientProcessor processor(sock.get());
	while (true)
	{
		std::cout << "Processing client...." << std::endl;
		processor.ProcessClient();
	}
}

int TcpServerMain()
{
	/*

		std::cout << "Enter address" << std::endl;
		std::cin >> address;

		std::string port;
		std::cout << "Enter port" << std::endl;
		std::cin >> port;
		
	*/

	std::string address = "192.168.179.128";
	std::string port = "5555";

	CTcpSocket server;
	if (!server.StartListen(address, port, 0))
	{
		std::cout << "Failed to start listen" << std::endl;
		return -1;
	}

	while (true)
	{
		std::shared_ptr<CTcpSocket> client(std::make_shared<CTcpSocket>());
		if (!server.Accept(*client))
			return errno;
		std::thread([client] () { CClientProcessor(client.get()).ProcessClient(); }).detach();
	}
}

int main(int argc, char * argv[])
{
	/*

	char chTcp = 'n';
	std::cout << "Tcp or Udp? (t/u) << std::endl;
	bool bTcp = ::tolower(chTcp) == 't';
	*/

	Startup();
	while (true)
	{
		bool bTcp = false;

	#ifdef CLIENT
		Client(bTcp);
	#else
		if (bTcp)
			return TcpServerMain();
		else
			UdpServerMain();
	#endif
	}

	Cleanup();
	return 0;
}
