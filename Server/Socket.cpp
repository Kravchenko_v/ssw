#include "Socket.h"

bool CSocket::Recv(void * pBuffer, size_t bufferSize)
{
	int intBufferSize = static_cast<int>(bufferSize);
	int receivedBytes = 0;
	do
	{
		int bytesRead = RecvImpl(reinterpret_cast<char *>(pBuffer) + receivedBytes,
								 intBufferSize - receivedBytes, 0);
		if (bytesRead == -1)
			return false;
		receivedBytes += bytesRead;
	} while (receivedBytes != intBufferSize);

	return true;
}

bool CSocket::Recv(std::string & recvString, char terminator)
{
	const int startSize = 260;
	typedef std::vector<char> buffer_t;
	buffer_t buffer(startSize);
	buffer_t::iterator itTerminator;
	do
	{
		int received = RecvImpl(&buffer[0], static_cast<int>(buffer.size()), MSG_PEEK);
		if (received <= 0)
			return false;
		else if (received == buffer.size())
			buffer.resize(buffer.size() + startSize);
	} while ((itTerminator = std::find(buffer.begin(), buffer.end(), terminator)) == buffer.end());
	size_t nBytes = std::distance(buffer.begin(), itTerminator) + 1;
	buffer.resize(nBytes);
	RecvImpl(&buffer[0], buffer.size(), 0);
	buffer.back() = '\0';
	recvString = &buffer[0];
	return true;
}

bool CSocket::Send(const void * buffer, size_t size)
{
	const char * charBuffer = static_cast<const char *>(buffer);
	int nBytesSend = 0;
	int nBytesRemaining = static_cast<int>(size);
	while (nBytesRemaining != 0)
	{
		int nRes = SendImpl(charBuffer + nBytesSend, nBytesRemaining, 0);
		if (nRes < 0)
			return false;
		nBytesSend += nRes;
		nBytesRemaining -= nRes;
	}
	return true;
}

bool CSocket::Send(const std::string & message)
{
	std::string text = message + "\r\n";
	return Send(text.c_str(), text.size());
}

const int sendRecvTimeout = 30;

int CSocket::SetTimeouts(SOCKET sock, unsigned int seconds)
{
#ifndef _WIN32
	timeval timeVal = { 0 };
	timeVal.tv_sec = seconds;
#else
	DWORD timeVal = seconds * 1000;
#endif
	int err = ::setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO,
						   reinterpret_cast<const char *>(&timeVal),
						   sizeof timeVal);
	err |= ::setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO,
						reinterpret_cast<const char *>(&timeVal),
						sizeof timeVal);
	return err;
}