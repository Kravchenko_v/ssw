#include "Common.h"

void Startup()
{
#ifdef _WIN32
	WSADATA data;
	::WSAStartup(MAKEWORD(2, 2), &data);
#else
#endif
}

void Cleanup()
{
#ifdef _WIN32
	::WSACleanup();
#else
#endif
}

uint64_t GetFileSize(const std::string & filename)
{
	std::ifstream inFile(filename, std::ios::ate | std::ios::binary);
	return inFile.is_open() ? inFile.tellg() : uint64_t(-1);
}

Command ParseCommand(const std::string & message, std::string & args)
{
	std::string command;
	for (size_t i = 0; i < message.size(); ++i)
	{
		if (message[i] != ' ' && message[i] != '\t'
			&& message[i] != '\r' && message[i] != '\n')
			continue;

		command = message.substr(0, i);
		args = message.substr(i + 1);
		break;
	}

	while (!args.empty() && (args.back() == '\r' || args.back() == '\n'))
		args.pop_back();

	std::string lowerCommand;
	lowerCommand.resize(command.size());
	std::transform(command.begin(), command.end(), lowerCommand.begin(), ::tolower);

	if (lowerCommand.empty())
		return EMPTY;
	if (lowerCommand == "time")
		return TIME;
	if (lowerCommand == "echo")
		return ECHO;
	if (lowerCommand == "exit")
		return EXIT;
	if (lowerCommand == "upload")
		return UPLOAD;
	if (lowerCommand == "download")
		return DOWNLOAD;
	return UNKNOWN;
}
