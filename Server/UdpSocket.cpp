#include "UdpSocket.h"
#include "Timestamp.h"

CUdpSocket::CUdpSocket(unsigned short destinationPort, unsigned short localPort)
	: m_sock(::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))
{
	int bBroadcast = 1;
	::setsockopt(m_sock, SOL_SOCKET, SO_BROADCAST, reinterpret_cast<const char *>(&bBroadcast), sizeof bBroadcast);
	sockaddr_in recvAddr = { 0 };
	recvAddr.sin_family = AF_INET;
	recvAddr.sin_port = ::htons(localPort);
	recvAddr.sin_addr.S_un.S_addr = INADDR_ANY;
	m_sendAddr = { 0 };
	m_sendAddr.sin_family = AF_INET;
	m_sendAddr.sin_port = ::htons(destinationPort);
	m_sendAddr.sin_addr.S_un.S_addr = ::inet_addr("192.168.179.255");
	int len = sizeof recvAddr;
	::bind(m_sock, reinterpret_cast<const sockaddr *>(&recvAddr), len);
	SetTimeouts(m_sock, 3);
}

const size_t payloadSize = 512;
typedef char Portion[504];
struct UdpFileSendPacket
{
	uint64_t fileOffset;
	Portion payload;
};

typedef uint64_t UdpFileIdPacket;
struct UdpStatusPacket
{
	UdpFileIdPacket id;
	uint64_t currentOffset;
};

bool CUdpSocket::RecvFile(const std::string & filename, size_t & receivedPortions)
{
	CTimestampGuard timestamp("RecvFile");
	uint64_t fileSize = 0;
	if (!RecvConfirmation(fileSize, filename))
		return false;

	size_t portionsCount = static_cast<size_t>(fileSize / sizeof(Portion));
	size_t lastPortionSize = static_cast<size_t>(fileSize % sizeof(Portion));

	std::ofstream outFile(filename, std::ios::out | std::ios::binary);
	if (!outFile.is_open())
		std::cout << "Failed to open" << std::endl;

	outFile.seekp(uint64_t(receivedPortions) * sizeof(Portion));

	size_t nErrorCounter = 0;
	while (receivedPortions < portionsCount + 1)
	{
		if (receivedPortions % 5 == 0 || nErrorCounter > 0)
		{
			if (nErrorCounter > 30)
				return false;

			UdpStatusPacket statusPacket;
			statusPacket.currentOffset = outFile.tellp();
			statusPacket.id = fileSize;
			if (!Send(&statusPacket, sizeof statusPacket))
				++nErrorCounter;
		}

		UdpFileSendPacket packet;
		auto currentOffset = outFile.tellp();
		if (!Recv(&packet, sizeof packet) || uint64_t(currentOffset) < packet.fileOffset)
		{
			++nErrorCounter;
			continue;
		}
		if (!(rand() % 3) || uint64_t(currentOffset) > packet.fileOffset)
			continue;
		
		nErrorCounter = 0;
		outFile.write(reinterpret_cast<const char *>(&packet.payload), (receivedPortions == portionsCount ?
					  lastPortionSize : sizeof packet.payload));
		++receivedPortions;
	}

	timestamp.SetBytes(fileSize);
	return true;
}

bool CUdpSocket::SendFile(const std::string & filename, size_t & sentPortions)
{
	uint64_t fileSize = 0;
	CTimestampGuard timestamp("SendFile");
	if (!SendConfirmation(fileSize, filename))
		return false;

	size_t portionsCount = static_cast<size_t>(fileSize / sizeof(Portion));
	size_t lastPortionSize = static_cast<size_t>(fileSize % sizeof(Portion));

	std::ifstream inFile(filename, std::ios::in | std::ios::binary);
	if (!inFile.is_open())
		std::cout << "Failed to open" << std::endl;

	inFile.seekg(uint64_t(sentPortions) * sizeof(Portion));

	while (sentPortions < portionsCount + 1)
	{
		if (sentPortions % 5 == 0)
		{
			size_t nError = 0;
			for (; nError < 30; ++nError)
			{
				UdpStatusPacket status;
				if (!Recv(&status, sizeof status) || status.id != fileSize)
					continue;
				inFile.seekg(status.currentOffset);
				if (status.currentOffset == uint64_t(-1))
					break;
				sentPortions = size_t(status.currentOffset / sizeof Portion);
				break;
			}
			if (nError == 30)
				return false;
		}

		UdpFileSendPacket packet;
		packet.fileOffset = inFile.tellg();
		inFile.read(reinterpret_cast<char *>(&packet.payload), sizeof packet.payload);
		if (Send(&packet, sizeof packet))
			++sentPortions;
		else
			inFile.seekg(uint64_t(inFile.tellg()) - sizeof packet.payload);
	}

	timestamp.SetBytes(fileSize);
	return true;
}

CUdpSocket::~CUdpSocket()
{
	::closesocket(m_sock);
}

int CUdpSocket::RecvImpl(char * buf, int len, int flags) const
{
	sockaddr_in from = { 0 };
	int fromLen = sizeof from;
	return ::recvfrom(m_sock, buf, len, flags, reinterpret_cast<sockaddr *>(&from), &fromLen);
}

int CUdpSocket::SendImpl(const char * buf, int len, int flags) const
{
	int addrlen = sizeof m_sendAddr;
	return ::sendto(m_sock, buf, len, flags, reinterpret_cast<const sockaddr *>(&m_sendAddr), addrlen);
}

bool CUdpSocket::SendConfirmation(uint64_t & fileSize, const std::string & filename)
{
	fileSize = GetFileSize(filename);
	if (!Send(&fileSize, sizeof fileSize))
	{
		std::cout << "Failed to send file size" << std::endl;
		return false;
	}

	UdpFileIdPacket statusConfirmation;
	if (!Recv(&statusConfirmation, sizeof statusConfirmation))
	{
		std::cout << "Failed to recv confirmation" << std::endl;
		return false;
	}

	if (fileSize != statusConfirmation || fileSize == -1)
	{
		std::cout << "Bad confirmation" << std::endl;
		return false;
	}
	return true;
}

bool CUdpSocket::RecvConfirmation(uint64_t & fileSize, const std::string & filename)
{
	fileSize = 0;
	if (!Recv(&fileSize, sizeof fileSize))
	{
		std::cout << "Failed to recv file size" << std::endl;
		return false;
	}
	if (!Send(&fileSize, sizeof fileSize))
	{
		std::cout << "Failed to send confirmation" << std::endl;
		return false;
	}
	if (fileSize == -1)
	{
		std::cout << "Bad file size" << std::endl;
		return false;
	}
	return true;
}
