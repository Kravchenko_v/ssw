#pragma once
#include "Common.h"
#include "Socket.h"

class CClientProcessor
{
public:
	CClientProcessor(CSocket * socket);
	void ProcessClient();

private:
	bool Echo();
	bool Time();
	bool UnknownCommand();
	bool SendFile();
	bool RecvFile();
	bool SelectCommand();

private:
	CSocket * m_client;
	std::string m_message;
	std::string m_args;
};
