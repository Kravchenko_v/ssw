#pragma once
#include "Socket.h"

class CUdpSocket : public CSocket
{
public:
	CUdpSocket(unsigned short destinationPort, unsigned short localPort);
	virtual bool RecvFile(const std::string & filename, size_t & receivedPortions) override;
	virtual bool SendFile(const std::string & filename, size_t & sentPortions) override;

	~CUdpSocket();

private:
	virtual int RecvImpl(char * buf, int len, int flags) const override;
	virtual int SendImpl(const char * buf, int len, int flags) const override;

	bool SendConfirmation(uint64_t & fileSize, const std::string & filename);
	bool RecvConfirmation(uint64_t & fileSize, const std::string & filename);
private:
	SOCKET m_sock;
	sockaddr_in m_sendAddr;
};