#pragma once
#include "Common.h"

class CTimestampGuard
{
public:
	CTimestampGuard(const std::string & commandType = std::string());
	~CTimestampGuard();
	void SetBytes(uint64_t nBytes);
private:
	uint64_t m_nBytes;
	clock_t m_start;
};