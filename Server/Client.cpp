#include "Client.h"
#include "TcpSocket.h"
#include "UdpSocket.h"

class CClient
{
public:
	CClient(const std::string & address, const std::string & port)
		: m_address(address)
		, m_port(port)
		, m_bTcp(true)
	{
			Reopen();
	}
	CClient(unsigned short serverPort, unsigned short localPort)
	{
		m_sock.reset(new CUdpSocket(serverPort, localPort));
	}

	bool Loop()
	{
		while (true)
		{
			std::string message;
			std::cout << "Enter message" << std::endl;
			std::getline(std::cin, message);
			if (message.empty())
				continue;

			std::string args;
			Command command = ParseCommand(message + '\n', args);
			if (!m_sock->Send(message))
			{
				std::cout << (m_bTcp? "Connection lost" : "Failed to send" ) << std::endl;
				Reopen();
				continue;
			}
			switch (command)
			{
				case TIME:
				case ECHO:
				case UNKNOWN:
					m_sock->Recv(message, '\n');
					std::cout << (message + '\n');
				break;
				case EXIT:
					return false;
				case UPLOAD:
					ProcessFile(args, true);
				break;
				case DOWNLOAD:
					ProcessFile(args, false);
				break;
			}
		}
	}

private:
	bool Reopen()
	{
		if (!m_bTcp)
			return true;

		auto sock = new CTcpSocket();
		bool retval = sock->Connect(m_address, m_port);
		if (!retval)
			std::cout << "Failed to connect" << std::endl;
		m_sock.reset(sock);
		return retval;
	}

	bool TryAgain()
	{
		std::cout << "Connection lost. Try again? (Y/N)" << std::endl;
		while (true)
		{
			char c = 0;
			std::cin >> c;
			switch (c)
			{
				case 'y':
				case 'Y':
					return true;
				case 'N':
				case 'n':
					return false;
			}
		}
	}

	bool ProcessFile(const std::string & filename, bool bSend)
	{
		size_t nPortions = 0;
		bool bSuccess = true;
		while (true)
		{
			if (bSuccess)
				bSuccess = m_sock->Send(&nPortions, sizeof nPortions);
			if (bSuccess)
			{
				if (bSend)
					bSuccess = m_sock->SendFile(filename, nPortions);
				else
					bSuccess = m_sock->RecvFile(filename, nPortions);
			}

			if (bSuccess)
				return true;
			else if (!TryAgain())
				return false;
			bSuccess = Reopen();
			if (bSuccess && m_bTcp)
				bSuccess = m_sock->Send(std::string(bSend ? "upload " : "download ") + filename);
		}

	}

private:
	std::unique_ptr<CSocket> m_sock;
	std::string m_address;
	std::string m_port;
	bool m_bTcp;
};

bool Client(bool bTcp)
{
	if (bTcp)
	{
		std::string address = "192.168.179.128";
		std::string port = "5555";
		/*

		std::cout << "Enter address" << std::endl;
		std::cin >> address;

		std::string port;
		std::cout << "Enter port" << std::endl;
		std::cin >> port;

		*/
		return CClient(address, port).Loop();
	}
	else
	{
		std::string portClientStr = "5555";
		std::string portServerStr = "5556";

		/*
		std::cout << "Enter server port " << std::endl;
		std::cin << portServerStr;
		std::cout << "Enter client port " << std::endl;
		std::cin << portClientStr;
		*/
		auto portClient = unsigned short(std::stoul(portClientStr));
		auto portServer = unsigned short(std::stoul(portServerStr));
		return CClient(portServer, portClient).Loop();
	}
}