#pragma once
#include "Common.h"

class CFileLock
{
public:
	CFileLock(const std::string & filename, bool bRead);
	~CFileLock();

private:
	std::string m_filename;

	static std::set<std::string> s_names;
	static std::mutex s_namesLock;
};