#pragma once
#include "Common.h"
#include "Socket.h"

class CTcpSocket : public CSocket
{
public:
	explicit CTcpSocket(SOCKET sock = INVALID_SOCKET);

	bool StartListen(const std::string & address, const std::string & port, int nClients);
	bool Connect(const std::string & address, const std::string & port);
	bool Accept(CTcpSocket & sock);

	bool IsOpen() const;
	void Close();
	
	virtual bool RecvFile(const std::string & filename, size_t & receivedPortions) override;

	virtual bool SendFile(const std::string & filename, size_t & sentPortions) override;

	~CTcpSocket();

private:
	virtual int RecvImpl(char * buf, int len, int flags) const override;
	virtual int SendImpl(const char * buf, int len, int flags) const override;

	bool OpenDefaultTcp();
	SOCKET m_sock;
};