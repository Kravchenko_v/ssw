#include "FileLock.h"

std::set<std::string> CFileLock::s_names;
std::mutex CFileLock::s_namesLock;

CFileLock::CFileLock(const std::string & filename, bool bRead)
	: m_filename(filename)
{
	std::unique_lock<std::mutex> lock(s_namesLock);
	std::condition_variable cv;
	cv.wait(lock, [&] ()
	{
		return s_names.insert(m_filename).second;
	});
}


CFileLock::~CFileLock()
{
	std::lock_guard<std::mutex> guard(s_namesLock);
	s_names.erase(m_filename);
}
