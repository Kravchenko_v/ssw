#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <vector>
#include <set>
#include <iterator>
#include <algorithm>

#include <memory>

#include <thread>
#include <mutex>
#include <condition_variable>

#include <cstdlib>
#include <ctime>


#ifdef _WIN32
#include <ws2tcpip.h>
typedef ADDRINFO addrinfo;
#else
typedef int SOCKET;
#endif

void Startup();
void Cleanup();
uint64_t GetFileSize(const std::string & filename);

enum Command
{
	  EMPTY
	, UNKNOWN
	, TIME
	, ECHO
	, EXIT
	, UPLOAD
	, DOWNLOAD
};

Command ParseCommand(const std::string & message, std::string & args);
