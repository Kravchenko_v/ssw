#include "ClientProcessor.h"
#include "Timestamp.h"
#include "FileLock.h"

CClientProcessor::CClientProcessor(CSocket * socket) 
	: m_client(socket)
{
}

void CClientProcessor::ProcessClient()
{
	while (m_client->Recv(m_message, '\n') && SelectCommand())
		;
}

bool CClientProcessor::Echo()
{
	CTimestampGuard guard("Echo");
	return m_client->Send(m_args);
}

bool CClientProcessor::Time()
{
	CTimestampGuard guard("Time");
	std::time_t time;
	char buffer[80];

	std::time(&time);
	std::tm * timeinfo = std::localtime(&time);

	std::strftime(buffer, sizeof buffer, "%d-%m-%Y %I:%M:%S", timeinfo);
	return m_client->Send(buffer);
}

bool CClientProcessor::UnknownCommand()
{
	return m_client->Send("Unknown command");
}

bool CClientProcessor::SendFile()
{
	CFileLock lock(m_args, true);
	size_t sent = 0;			//only client can continue download/upload
	return m_client->Recv(&sent, sizeof sent) && m_client->SendFile(m_args, sent);
}

bool CClientProcessor::RecvFile()
{
	CFileLock lock(m_args, false);
	size_t received = 0;
	return m_client->Recv(&received, sizeof received) && m_client->RecvFile(m_args, received);
}

bool CClientProcessor::SelectCommand()
{
	std::cout << "Message: " << m_message << std::endl;
	Command command = ParseCommand(m_message, m_args);
	switch (command)
	{
		case TIME:
			return Time();
		case ECHO:
			return Echo();
		case EXIT:
			return false;
		case EMPTY:
			return true;
		case UPLOAD:
			return RecvFile();
		case DOWNLOAD:
			return SendFile();
	}
	return UnknownCommand();
}
