#include "TcpSocket.h"
#include "Timestamp.h"

CTcpSocket::CTcpSocket(SOCKET sock)
	: m_sock(sock)
{
}

namespace
{
	addrinfo * GetAddrInfo(const std::string & address, const std::string & port)
	{
		addrinfo hints = { 0 };
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;
		addrinfo * pAddrinfo = nullptr;
		int err = ::getaddrinfo(address.c_str(), port.c_str(), &hints, &pAddrinfo);
		if (err == SOCKET_ERROR)
			return nullptr;

		return pAddrinfo;
	}
}

bool CTcpSocket::StartListen(const std::string & address, const std::string & port, int nClients)
{
	if (!OpenDefaultTcp())
		return false;

	addrinfo * pAddrinfo = GetAddrInfo(address, port);
	if (!pAddrinfo)
		return false;

	int err = ::bind(m_sock, pAddrinfo->ai_addr, static_cast<int>(pAddrinfo->ai_addrlen));

	err |= SetTimeouts(m_sock, 30);
	err |= ::listen(m_sock, nClients);
	::freeaddrinfo(pAddrinfo);
	
	if (err == SOCKET_ERROR)
	{
		Close();
		return false;
	}
	return true;
}

bool CTcpSocket::Connect(const std::string & address, const std::string & port)
{
	if (!OpenDefaultTcp())
		return false;

	addrinfo * pAddrinfo = GetAddrInfo(address, port);
	if (!pAddrinfo)
		return false;

	int err = ::connect(m_sock, pAddrinfo->ai_addr, static_cast<int>(pAddrinfo->ai_addrlen));
	err |= SetTimeouts(m_sock, 30);
	::freeaddrinfo(pAddrinfo);

	if (err == SOCKET_ERROR)
	{
		Close();
		return false;
	}

	return true;
}

bool CTcpSocket::Accept(CTcpSocket & sock)
{
	sock.Close();
	sock.m_sock = ::accept(m_sock, nullptr, nullptr);
	return sock.IsOpen();
}

bool CTcpSocket::IsOpen() const
{
	return m_sock != INVALID_SOCKET;
}

void CTcpSocket::Close()
{
	if (!IsOpen())
		return;

	::closesocket(m_sock);
	m_sock = INVALID_SOCKET;
}

namespace
{
	typedef char Portion[4096];

}

bool CTcpSocket::RecvFile(const std::string & filename, size_t & receivedPortions)
{
	CTimestampGuard timestamp("RecvFile");
	uint64_t fileSize = 0;
	if (!Recv(&fileSize, sizeof fileSize))
		return true;

	if (fileSize == uint64_t(-1))
		return true;

	size_t portionsCount = static_cast<size_t>(fileSize / sizeof(Portion));
	size_t lastPortionSize = fileSize % sizeof(Portion);

	std::ios::openmode openmode = (receivedPortions == 0) ?
		std::ios::trunc : std::ios::ate;
	std::ofstream outFile(filename, std::ios::out | std::ios::binary | openmode);
	if (!outFile.is_open())
		std::cout << "Failed to open" << std::endl;

	outFile.seekp(uint64_t(receivedPortions) * sizeof(Portion));
	
	for (size_t i = receivedPortions; i < portionsCount; ++i)
	{
		Portion portion;
		if (!Recv(portion, sizeof portion))
			return false;
		outFile.write(portion, sizeof portion);
	}

	if (lastPortionSize)
	{
		std::vector<char> lastPortion(lastPortionSize);
		if (!Recv(&lastPortion[0], lastPortionSize))
			return false;
		outFile.write(&lastPortion[0], lastPortionSize);
	}
	timestamp.SetBytes(fileSize);
	return true;
}

bool CTcpSocket::SendFile(const std::string & filename, size_t & sentPortions)
{
	CTimestampGuard timestamp("SendFile");
	uint64_t fileSize = GetFileSize(filename);
	if (!Send(&fileSize, sizeof fileSize))
		return false;

	if (fileSize == uint64_t(-1))
		return true;

	size_t portionsCount = static_cast<size_t>(fileSize / sizeof(Portion));
	size_t lastPortionSize = static_cast<size_t>(fileSize % sizeof(Portion));

	std::ifstream inFile(filename, std::ios::in | std::ios::binary);
	if (!inFile.is_open())
		std::cout << "Failed to open" << std::endl;

	inFile.seekg(uint64_t(sentPortions) * sizeof(Portion));

	for (; sentPortions < portionsCount; ++sentPortions)
	{
		Portion portion;
		inFile.read(portion, sizeof portion);
		if (!Send(portion, sizeof portion))
			return false;
	}
	
	if (lastPortionSize)
	{
		std::vector<char> lastPortion(lastPortionSize);
		inFile.read(&lastPortion[0], lastPortionSize);
		return Send(&lastPortion[0], lastPortionSize);
	}
	timestamp.SetBytes(fileSize);
	return true;
}

CTcpSocket::~CTcpSocket()
{
	Close();
}

bool CTcpSocket::OpenDefaultTcp()
{
	Close();
	m_sock = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	return m_sock != INVALID_SOCKET;
}

int CTcpSocket::RecvImpl(char * buf, int len, int flags) const
{
	return ::recv(m_sock, buf, len, flags);
}

int CTcpSocket::SendImpl(const char * buf, int len, int flags) const
{
	return ::send(m_sock, buf, len, flags);
}

